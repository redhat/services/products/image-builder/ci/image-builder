# CI for image-builder-crc project

NOTE: The upstream repository was renamed from `image-builder` to
`image-builder-crc` so this repository is also subject to rename
and adapt all references (e.g. see `trigger-gitlab.yml` below)

This repository essentially is a mirror of https://github.com/osbuild/image-builder-crc

For each PR on github a _new branch_ `PR-*` is created here to run CI, see trigger here:
https://github.com/osbuild/image-builder-crc/blob/main/.github/workflows/trigger-gitlab.yml
